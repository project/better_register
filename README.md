# Features

* Route /user/edit to redirect the user to the edit account page (/user/\<uid>/edit) or 
  to the login page (user/login) if annonymous.
